-- a class for grids
-- and some functions for managing them

local TracksGrid = Class("TracksGrid")

function TracksGrid:initialize(config)
    self.config = config
    -- size of the playing field (number of tiles)
    self.width = config.width
    self.height = config.height
    self.color = config.playerColor
    self.tiles = {}
    self.trackImage = Images.foot_tracks

    -- init tiles
    for x = 1, self.width do
        self.tiles[x] = {}
        for y = 1, self.height do
            self.tiles[x][y] = {inDir = nil, outDir = nil}
        end
    end
end

function TracksGrid:addStep(x, y, inDir, outDir)
    if inDir ~= nil then
        self.tiles[x][y].inDir = inDir
    end
    if outDir ~= nil then
        self.tiles[x][y].outDir = outDir
    end
end

function TracksGrid:angleBetween(dir1, dir2)
    local angle1 = dir1.rotationAngle
    local angle2 = dir2.rotationAngle
    if angle2 > angle1 + math.pi then
        angle1 = angle1 + math.pi * 2
    elseif angle1 > angle2 + math.pi then
        angle2 = angle2 + math.pi * 2
    end
    return (angle1 + angle2) / 2
end

function TracksGrid:draw()
    
    local feet = self.trackImage

    local scaleX = 0.2 / feet:getWidth()
    local scaleY = scaleX

    local trackColor = {self.color[1], self.color[2], self.color[3], 0.5}
    Daylight:applyReflColor(unpack(trackColor))
    for x = 1, self.width do
        for y = 1, self.height do
            local px = x - 1
            local py = y - 0.8
            local inDir = self.tiles[x][y].inDir
            local outDir = self.tiles[x][y].outDir
            local direction1 = DOWN

            if inDir ~= nil then
                -- compute start track place and rotation
                local placeX = px + 0.5 - inDir.x * 0.35
                local placeY = py  + 0.5 - inDir.y * 0.35
                direction1 = inDir
                -- draw start track
                love.graphics.draw(
                    feet,
                    placeX,
                    placeY,
                    direction1.rotationAngle,
                    scaleX,
                    scaleY,
                    0.5 * feet:getWidth(),
                    0.5 * feet:getHeight()
                )
            end
            if outDir ~= nil then
                -- compute end track place and rotation
                local placeX = px + 0.5 + outDir.x * 0.35
                local placeY = py + 0.5 + outDir.y * 0.35
                local direction2 = outDir

                -- draw end track
                love.graphics.draw(
                    feet,
                    placeX,
                    placeY,
                    direction2.rotationAngle,
                    scaleX,
                    scaleY,
                    0.5 * feet:getWidth(),
                    0.5 * feet:getHeight()
                )
                -- compute middle track rotation
                if inDir ~= nil then
                    local direction3Angle = 0

                    if inDir == outDir then
                        direction3Angle = inDir.rotationAngle
                        placeX = px + 0.5
                        placeY = py + 0.5
                    else
                        placeY = py + 0.5 - inDir.y * 0.125
                        placeX = px + 0.5 - inDir.x * 0.125
                        if inDir.vec:isInverse(outDir.vec) then
                            direction3Angle = inDir.cw.rotationAngle
                            if inDir == UP or inDir == DOWN then
                                placeX = px + 0.5
                            else
                                placeY = py + 0.5
                            end
                        else
                            direction3Angle = self:angleBetween(inDir, outDir)
                            if outDir == LEFT then
                                placeX = placeX - 0.1
                            elseif outDir == RIGHT then
                                placeX = placeX + 0.1
                            elseif outDir == UP then
                                placeY = placeY - 0.1
                            elseif outDir == DOWN then
                                placeY = placeY + 0.1
                            end
                        end
                    end

                    -- draw middle track
                    love.graphics.draw(
                        feet,
                        placeX,
                        placeY,
                        direction3Angle,
                        scaleX,
                        scaleY,
                        0.5 * feet:getWidth(),
                        0.5 * feet:getHeight()
                    )
                end
            end
        end
    end
end

return TracksGrid
