local name = "Level Editor"
local config = {
    editMode = true,
    intro = "This is the Level Editor.\n\n"..
        "(Press "..Input:getKeyString("click").." to start)",
    preOutro = "Phew, that was stressful!",
    outro = "Maybe I'm getting too old for this kind of work...",
    homes = {
        {x = 5, y = 5},
    },
    objects = {
        {name = 'fruit', x = 2, y = 3},
        {name = 'fruit', x = 3, y = 3},
        {name = 'fruit', x = 4, y = 3},
        {name = 'fruit', x = 2, y = 4},
        {name = 'fruit', x = 3, y = 4},
        {name = 'fruit', x = 4, y = 4},
    },
    obstacles = {
        {x = 2, y = 3, top = 'wall', right = nil},
        {x = 3, y = 4, top = 'wall', right = 'wall'},
    },
    width = 10,
    height = 10,
    arrows = {
        {
            color = {1,1,1,1},
            count = 5,
            fixed = {
                {
                    x = 3,
                    y = 1,
                    direction = "down",
                }
            },
        },
        {
            color = {1,1,0,1},
            count = 3,
        },
    },
    golems = {
        {
            count = 0,
            fixed = { 
                {
                    color = {1,1,0,1},
                    x = 5,
                    y = 1,
                    direction = "left",
                }
            },
        },
    },
    robots = 0,
    stepsLimit = 8,
}

local level = LevelEditor:new(name, config)

return level