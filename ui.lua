Button = require "button"
local Ui = Class("Ui")

function Ui:initialize()
	self.widgets = {}
	self.currentY = 20
	self.currentX = 1080
	self.defaultRowHeight = 50
	self.rowHeight = self.defaultRowHeight
end

function Ui:update()
	for _, widget in pairs(self.widgets) do
		widget:update()
	end
end

function Ui:addButton(button, dontPosition)
	table.insert(self.widgets, button)
	
	if dontPosition then
		return
	end
	if self.currentX + button.width > 1900 then
		self:newRow()
	end

	button.x = self.currentX
	button.y = self.currentY
	
	self.currentX = self.currentX + 10 + button.width
	if button.height > self.rowHeight then
		self.rowHeight = button.height
	end
end

function Ui:newRow()
	self.currentX = 1080
	self.currentY = self.currentY + self.rowHeight + 10

	self.rowHeight = self.defaultRowHeight
end

function Ui:draw()
	for _, widget in pairs(self.widgets) do
		widget:draw()
	end
end

function Ui:addSelectButtonGroup(buttons, callback, dontPosition)
	if self.currentX > 1080 and not dontPosition then
		self:newRow()
	end
	for _, button in pairs(buttons) do
		button.isToggleButton = true
		if not dontPosition then
			self:addButton(button)
		end
		button.onSelect = function(clickedButton)
			for _, otherButton in pairs(buttons) do
				if clickedButton ~= otherButton then
					otherButton.isSelected = false
				end
			end
			if callback then
				callback(button)
			end
		end

		button.onDeselect = function(clickedButton)
			if callback then
				callback(nil)
			end
		end
	end
	self:newRow()
end

return Ui
