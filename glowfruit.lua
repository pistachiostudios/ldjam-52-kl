local GlowFruit = Class("GlowFruit")

function GlowFruit:initialize(maxHealth)
    self.color = {r=0.5, g=0.5, b=1}
    self.maxHealth = maxHealth
    self.health = maxHealth
    self.image1 = Images.glowfruit_glow_big
    self.image2 = Images.glowfruit_glow_medium
    self.rotation = math.random() * 2 * math.pi --random rotation
    self.maxAnimationTime = 2
    self.animationTime = (math.random()) -- start with a random time shift
end

function GlowFruit:update(dt)
    self.animationTime = self.animationTime + dt
    if self.animationTime >= self.maxAnimationTime then
        self.animationTime = 0
    end
end

function GlowFruit:setColor(r, g, b)
    self.color = {r=r, g=g, b=b}
end

function GlowFruit:decrementHealth()
    self.health = self.health - 1
    if self.health <= 0 then
        self:setColor(0.2, 0.05, 0.1)
        self.image1 = Images.glowfruit
        self.image2 = Images.glowfruit
    elseif self.health <= 1 then
        self:setColor(0.3, 0.2, 0.6)
        self.image1 = Images.glowfruit
        self.image2 = Images.glowfruit_glow_small
    elseif self.health <= 2 then
        self:setColor(0.4, 0.3, 0.8)
        self.image1 = Images.glowfruit_glow_small
        self.image2 = Images.glowfruit_glow_medium
    elseif self.health <= 4 then
        self:setColor(0.45, 0.4, 0.9)
    else
        self:setColor(0.5, 0.5, 1)
    end
end

function GlowFruit:draw(x, y)
    -- x and y are grid coordinates

    local opacity1 = 0.5 + 0.5 * math.cos((self.animationTime) * math.pi)
    local opacity2 = 1 - opacity1

    local fruitX = x - 0.25
    local fruitY = y - 0.75

    local scale = 0.0005
    love.graphics.setBlendMode('alpha')
    Daylight:applyIllumColor(self.color.r, self.color.g, self.color.b, 1)
    love.graphics.draw(Images.glowfruit, fruitX, fruitY, self.rotation, scale, scale, 300, 300)
    if self.health > 0 then
        love.graphics.setBlendMode('add')
        Daylight:applyIllumColor(self.color.r, self.color.g, self.color.b, opacity1)
        love.graphics.draw(self.image1, fruitX, fruitY, self.rotation, scale, scale, 300, 300)
        Daylight:applyIllumColor(self.color.r, self.color.g, self.color.b, opacity2)
        love.graphics.draw(self.image2, fruitX, fruitY, self.rotation, scale, scale, 300, 300)
        love.graphics.setBlendMode('alpha')
    end

end

return GlowFruit