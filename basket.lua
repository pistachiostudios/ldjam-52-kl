local Basket = Class("Basket")

function Basket:initialize(capacity)
    self.capacity = capacity
    self.itemsCount = 0
end

function Basket:addItem()
    if self.itemsCount < self.capacity then
        self.itemsCount = self.itemsCount + 1
        return true
    else
        return false
    end
end

function Basket:isFull()
    return self.itemsCount == self.capacity
end

function Basket:clear()
    self.itemsCount = 0
end

return Basket