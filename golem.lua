local Basket = require "basket"
local Golem = Class("Golem")

--function Golem:initialize(level, x, y, direction, basketSize, color, name)
function Golem:initialize(level, config)
	config.name = config.name or 'child'
	config.basket = config.basket or 6
	-- print("new golem " .. tostring(config.name) .. " with basketSize " .. tostring(config.basket))
	-- position in grid coordinates
	self.level = level
	self.color = config.color or {1,1,1,1}
	self.pos = Vector.new(config.x, config.y)
	self.direction = DirectionMapping[config.direction] or table.randomValue(Directions)
	self.name = config.name
	if self.name == 'witch' then
		self.image = Images.witch
	else
		self.image = Images.child
	end
	self.mileage = 0.0
	self.moving = false
	self.tracks_grid = TracksGrid:new(level.config)
	self.tracks_grid.color = self.color
	self.basket = Basket:new(config.basket)
	self.speed = config.speed or 1
end

function Golem:reset()
	self.moving = false
	if self.startPos then
		self.pos = self.startPos
	end
	if self.startDirection then
		self.direction = self.startDirection
	end
	self.mileage = 0
	self.tracks_grid = TracksGrid:new(self.level.config)
	self.tracks_grid.color = self.color
	self.basket:clear()
end

function Golem:start() 
	self.startPos = self.pos
	self.startDirection = self.direction
	self.moving = true
end

function Golem:isAtHome()
    for i , home in ipairs(self.level.homes) do
        if self.pos.x == home.x and self.pos.y == home.y then
            return true
        end
    end
    return false
end

function Golem:hasFruitToPick()
	-- this is mostly the same logic as in `update` at `if self.mileage >= 1.0 then`
	-- but working on p and d to not change the real position and direction
	local p = self.pos:clone()
	local d = self.direction

	--print("check path for golem: " .. tostring(self.name) .. "...")
	local counter = 0
	repeat
		-- to avoid infinite loops
		counter = counter + 1
		if counter > self.level.config.width * self.level.config.height then
			return false
		end

		-- check fruit on current tile
		if self.level.fruits[p.x][p.y] then
			return true
		end

		local prevP = p -- remember the previous position
		-- move to next tile. This might contradict some obstacle, we check that next...
		p = p + d.vec

		--print("reaches " .. p.x .. ", " .. p.y)

		if not self.level.grid:isWalkable(p.x, p.y) then
			--print("golem stops: " .. tostring(self.name) .. " reaches non-walkable position without any more fruit")
			return false -- golem would reach non-walkable tile before a fruit
		else
			local prevTile = self.level.obstaclesGrid.tiles[prevP.x][prevP.y]
			local nextTile = self.level.obstaclesGrid.tiles[p.x][p.y]
			if d.x > 0 and prevTile.right
			or d.x < 0 and nextTile.right
			or d.y > 0 and nextTile.top
			or d.y < 0 and prevTile.top then
				--print("golem stops: " .. tostring(self.name) .. " reaches wall without any more fruit")
			  return false -- golem would run into a wall before a fruit
			end
		end
		-- if we reach this line, the previous move was ok

		-- check arrow on new tile
		local arrow = self.level.arrowGrid.arrows[p.x][p.y]
		if arrow and self:colorMatches(arrow.color) then
			d = arrow.direction
		end
	until p == self.pos
	
	-- golem has reached start position without finding fruit
	--print("golem stops: " .. tostring(self.name) .. " reaches current position without any more fruit")
	return false
end

function Golem:pickupObject()
	local x,y = self.pos.x, self.pos.y
    if self.level.fruits[x][y] then
        if self.basket:addItem() then
			Sounds.pick:setVolume(0.3)
            Sounds.pick:setPitch(0.7+math.random())
			Sounds.pick:play()

            -- remove object from the map and counter
            self.level.fruits[x][y] = false

            self.level.remaining_fruits = self.level.remaining_fruits -1
        elseif not self.level.showMessage and not ShownHintBasketFull then
            self.level.showMessage = HintBasketFull
            ShownHintBasketFull = true
			Sounds.oof:play()
        else
			Sounds.oof:play()
        end
    end
end

function Golem:update(dt)
	if self.moving then
		local prevMileage = self.mileage
		self.mileage = self.mileage + (dt * self.speed)

		-- make foot tracks
		if self.mileage > 0.15 and prevMileage <= 0.15 then
		--if self.mileage > 0.2 and prevMileage <= 0.2 then
			-- tracks going out of the current tile
			self.tracks_grid:addStep(self.pos.x, self.pos.y, nil, self.direction)
		end

		if self.mileage > 0.9 and prevMileage <= 0.9 then
			-- tracks going into the current tile
			self.tracks_grid:addStep(self.pos.x + self.direction.x, self.pos.y + self.direction.y, self.direction, nil)
		end

		if self.mileage >= 1.0 then -- new tile reached

			-- update actual posision
			self.mileage = self.mileage - 1.0
			self.pos = self.pos + self.direction.vec

			-- harvest
			self:pickupObject()

			-- check if golem is at home to empty the basket
			if self:isAtHome() then
				self.basket:clear()
				-- stay in the house if finished picking
				if not self:hasFruitToPick() then
					self.moving = false
				end
			end

			-- handle arrow on the new position
			local arrow = self.level.arrowGrid.arrows[self.pos.x][self.pos.y]
			if arrow and self:colorMatches(arrow.color) then
				self.direction = arrow.direction
			end

			-- check if the next tile that the golem would head to can be reached, stop moving otherwise
			local nextPos = self.pos  + self.direction.vec
			if not self.level.grid:isWalkable(nextPos.x, nextPos.y) then
				self.moving = false
			else
				local prevTile = self.level.obstaclesGrid.tiles[self.pos.x][self.pos.y]
				local nextTile = self.level.obstaclesGrid.tiles[nextPos.x][nextPos.y]
				if self.direction.x > 0 and prevTile.right
				or self.direction.x < 0 and nextTile.right
				or self.direction.y > 0 and nextTile.top
				or self.direction.y < 0 and prevTile.top then
				  self.moving = false
				end
			end
		end
	end
end

function Golem:colorMatches(color)
	if color[1] == 1 and color[2] == 1 and color[3] == 1 and color[4] == 1 then
		return true -- white always matches
	end
	if color[1] == self.color[1] and color[2] == self.color[2] and color[3] == self.color[3] and color[4] == self.color[4] then
		return true -- white always matches
	end
	return false
end

function Golem:draw(offsetX, offsetY, opacity)
    -- grid-based position
    local drawPos = self.pos - Vector.new(0.5, 0.5) + self.mileage * self.direction.vec

	local rotation = 0

	if self.name == 'witch' then
		if self.direction == DOWN then
			self.image = Images.witch
		elseif self.direction == UP then
			self.image = Images.witch_back
		elseif self.direction == LEFT then
			self.image = Images.witch_left
		elseif self.direction == RIGHT then
			self.image = Images.witch_right
		end

		drawPos.y = drawPos.y - 0.2
		Daylight:applyReflColor(unpack(self.color))
	elseif string.sub(self.name, 1, 5) == 'ghost'  then
		if self.direction == DOWN then
			self.image = Images.ghost_front_right_grayscale
		elseif self.direction == UP then
			self.image = Images.ghost_back_left_grayscale
		elseif self.direction == LEFT then
			self.image = Images.ghost_back_left_grayscale
		elseif self.direction == RIGHT then
			self.image = Images.ghost_front_right_grayscale
		end

		drawPos.y = drawPos.y - 0.2
		love.graphics.setBlendMode('add')

		Daylight:applyIllumColor(unpack(self.color))
	else -- other names
		rotation = self.direction.rotationAngle
		Daylight:applyReflColor(unpack(self.color))
	end


	-- this are the pixel lenghts of the graphic.
	local width = self.image:getWidth()
	local height = self.image:getHeight()

	local imageSize = math.max(width, height)
	
	local scale = 1 / imageSize
	love.graphics.draw(self.image, drawPos.x, drawPos.y, rotation, scale, scale, 0.5 * width, 0.5 * height)

	love.graphics.setBlendMode('alpha')
	if self.fixed and self.level.editMode then
		Daylight:applyAbstractColor(1,1,1,1)
		love.graphics.draw(Images.lock, drawPos.x, drawPos.y, 0, 1/32, 1/32, 16, 16)
	end

	local text = self.basket.itemsCount.."/"..self.basket.capacity
	
	love.graphics.pop()
	love.graphics.setFont(Fonts.small)
	Daylight:applyAbstractColor(1,1,1,1)
	local textX, textY = self.level.gridTransform:transformPoint(drawPos.x, drawPos.y - 0.8)
	love.graphics.printf(text, textX-50, textY, 100, "center")
	love.graphics.push()
	love.graphics.applyTransform(self.level.gridTransform)
end

return Golem