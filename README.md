# Grow In The Dark

*Made in 72 hours for [Ludum Dare 52 game jam](https://ldjam.com/events/ludum-dare/52/). The jam's theme was 'Harvest'.*

![Grow In The Dark - Title screen](https://static.jam.host/raw/e32/e/z/55c47.png)

**The old witch wants to pick all the glowing fruits in her garden tonight. Place some signs to help her find the way!**

## Controls

Place direction arrows with your `left mouse button`. Rotate the arrow that you will place with the `right mouse button`, `E` or `R`.

Mute the music with `M`.

Toggle fullscreen with `F`.

Open the level selection menu with `Q`.

Exit with `ESC`.

![Game screenshot](https://static.jam.host/raw/e32/e/z/55b1b.png)

## Misc

*Made with [LÖVE](https://love2d.org/). To run the Linux/macOS version, install LÖVE 11.3.*

### Credits

**Graphics, Programming, Level Design, Sound Effects:** 
- @Flauschzelle & @lenaschimmel

**Fonts:**
- [Milonga](https://fonts.google.com/specimen/Milonga) by Impallari Type 
- [Henny Penny](https://fonts.google.com/specimen/Henny+Penny) by Brownfox

**Music:**
- "Almost New" by [Kevin MacLeod](incompetech.com), *Licensed under [Creative Commons: By Attribution 4.0 License](http://creativecommons.org/licenses/by/4.0/)*

## Downloads

Windows 64 Bit: 
https://pistachiostudios.gitlab.io/Grow-In-The-Dark/downloads/pistachiostudios-grow-in-the-dark-win64.zip

Linux / MacOS:
https://pistachiostudios.gitlab.io/Grow-In-The-Dark/downloads/pistachiostudios-grow-in-the-dark.love