local Arrow = Class("Arrow")

function Arrow:initialize(direction, color)
    self.color = color
    self.direction = direction
end

function Arrow:draw(x, y)
    local image = Images.arrow_border

    if self.direction == UP then
        image = Images.arrow_sign_up
    elseif self.direction == DOWN then
        image = Images.arrow_sign_down
    elseif self.direction == LEFT then
        image = Images.arrow_sign_left
    elseif self.direction == RIGHT then
        image = Images.arrow_sign_right
    end

    Daylight:applyReflColor(unpack(self.color))
    -- grid-based position
    local posX = x - 0.8
    local posY = y - 0.75

    --local rotation = self.direction.rotationAngle

    -- this are the pixel lenghts of the graphic.
    local width = image:getWidth()
    local height = image:getHeight()
    local imageSize = math.max(width, height)
    
    local scale = 1/imageSize*0.65
    love.graphics.draw(image, posX, posY, 0, scale, scale, 0.5 * width, 0.5 * height)

    if self.fixed then
		Daylight:applyAbstractColor(1,1,1,1)
		love.graphics.draw(Images.lock, posX, posY, 0, 1/32, 1/32, 16, 16)
	end
end

return Arrow
