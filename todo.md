Bilder malen:
- Haus
- evtl. Animation
- Deko
- Hintergrundbild

Musik aussuchen:
- fürs pfeile-platzieren
- fürs laufen lassen

Programmieren:
- Eingabemethoden für Weg-Planungsphase überarbeiten

Level:
- Levelnamen und Texte
- mehr Level ausdenken und bauen
- Testspielen lassen

Abgabe:
- Titelbild basteln
- Credits-screen befüllen
- Abgabeseite ausfüllen
- Abgeben!


Done:
=====
- Boden-Texturen
- Bilder für Steine
- Bilder für Geister
- Bilder für Pfeile
- Bilder für Pflanzen
- Bild für Fußspuren
- Fußspuren blasser
- Zeitlimit
- golem im haus stehenbleiben lasen wenn keine früchte merh auf seinem weg liegen