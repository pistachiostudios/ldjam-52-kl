local name = "Homemade Potion"
local config = {
    intro = "I've found a slightly more durable kind of glowfruit!\n"..
    "But to be sure, I summoned three ghost friends to help me tonight.\n\n"..
    "I almost broke my ankle last time... so will just stay in the house and prepare for cooking glowfruit potion.\n"..
    "I'm sure my ghost friends will like it!\n",
    preOutro = "We made it, thanks for your help!",
    outro = "Would you also like some magic potion?",
    basket = 8,
    homes = {
        {x = 4, y = 4},
        {x = 6, y = 8},
    },
    objects = {
        {name = "fruit", x = 2, y = 1},
        {name = "fruit", x = 4, y = 1},
        {name = "fruit", x = 1, y = 2},
        {name = "fruit", x = 2, y = 2},
        {name = "fruit", x = 4, y = 2},
        {name = "fruit", x = 2, y = 3},
        {name = "fruit", x = 3, y = 3},
        {name = "fruit", x = 2, y = 4},
        {name = "fruit", x = 7, y = 4},
        {name = "fruit", x = 8, y = 4},
        {name = "fruit", x = 7, y = 5},
        {name = "fruit", x = 8, y = 5},
        {name = "fruit", x = 3, y = 6},
        {name = "fruit", x = 4, y = 6},
        {name = "fruit", x = 7, y = 6},
        {name = "fruit", x = 8, y = 6},
        {name = "fruit", x = 3, y = 8},
        {name = "fruit", x = 4, y = 8},
        {name = "fruit", x = 5, y = 8},
        {name = "fruit", x = 7, y = 8},
        {name = "fruit", x = 8, y = 8},
        {name = "fruit", x = 4, y = 9},
        {name = "fruit", x = 5, y = 9},
        {name = "fruit", x = 5, y = 10},
    },
    obstacles = {
        {x = 6, y = 1, top = nil, right = 'wall'},
        {x = 2, y = 2, top = 'wall', right = 'wall'},
        {x = 3, y = 2, top = 'wall', right = nil},
        {x = 1, y = 3, top = nil, right = 'wall'},
        {x = 6, y = 3, top = 'wall', right = nil},
        {x = 7, y = 5, top = 'wall', right = nil},
        {x = 1, y = 6, top = nil, right = 'wall'},
        {x = 8, y = 6, top = 'wall', right = nil},
        {x = 9, y = 7, top = nil, right = 'wall'},
        {x = 10, y = 7, top = 'wall', right = nil},
        {x = 4, y = 8, top = 'wall', right = nil},
        {x = 5, y = 8, top = 'wall', right = nil},
        {x = 5, y = 10, top = nil, right = 'wall'},
        {x = 2, y = 10, top = 'wall', right = nil},
        {x = 9, y = 10, top = 'wall', right = 'wall'},
    },
    width = 10,
    height = 10,
    stepsLimit = 12,
    arrows = {
        {
            color = {1,1,1,1},
            count = 22,
        }
    },
    golems = {
        {
            count = 0,
            fixed = {
                {
                    color = {0.5,0.5,1,1},
                    x = 5,
                    y = 4,
                    direction = "right",
                    name = "ghost1",
                    basket = 8,
                    speed = 1,
                },
                {
                    color = {1,0.5,0.5,1},
                    x = 3,
                    y = 4,
                    direction = "left",
                    name = "ghost2",
                    basket = 8,
                    speed = 1,
                },
                {
                    color = {0.5,1,0.5,1},
                    x = 6,
                    y = 8,
                    direction = "left",
                    name = "ghost3",
                    basket = 8,
                    speed = 1,
                },
            },
        },
    },
}

local level = Level:new(name, config)

return level