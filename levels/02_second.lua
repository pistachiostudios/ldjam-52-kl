local name = "Friendly Spirit"
local config = {
    intro = "The glowfruits are getting ripe again!\n"..
        "This time, I've summoned a friendly ghost to help me pick them.\n\n "..
        "But I still need to plan our ways in advance - there's no time for detours when the fruits are ready.\n",
    preOutro = "We harvested all the fruit tonight, and I didn't have to carry all of it alone!",
    outro = "I'm still very tired now. Maybe I should stop growing fruit that can only be picked at midnight...",
    basket = 6,
    homes = {
        {x = 4, y = 4},
    },
    objects = {
        {name = "fruit", x = 2, y = 2},
        {name = "fruit", x = 4, y = 2},
        {name = "fruit", x = 2, y = 3},
        {name = "fruit", x = 6, y = 3},
        {name = "fruit", x = 6, y = 4},
        {name = "fruit", x = 7, y = 5},
        {name = "fruit", x = 4, y = 6},
        {name = "fruit", x = 5, y = 6},
        {name = "fruit", x = 6, y = 6},
    },
    obstacles = {
        {x = 2, y = 3, top = nil, right = 'wall'},
        {x = 6, y = 5, top = nil, right = 'wall'},
    },
    width = 7,
    height = 7,
    stepsLimit = 18,
    arrows = {
        {
            color = {1,1,1,1},
            count = 8,
        },
    },
    golems = {
        {
            count = 0,
            fixed = {
                {
                    color = {1,1,1,1},
                    x = 3,
                    y = 4,
                    direction = "left",
                    name = "witch",
                    basket = 3,
                    speed = 0.5,
                },
                {
                    color = {0.7,0.7,1,1},
                    x = 5,
                    y = 4,
                    direction = "right",
                    name = "ghost1",
                    basket = 6,
                    speed = 1,
                },
            },
        },
    },
}

local level = Level:new(name, config)

return level