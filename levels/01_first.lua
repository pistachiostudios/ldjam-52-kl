local name = "Rare Fruits"
local config = {
    intro = "I grew some rare glowfruit plants in my garden, and they're finally carrying some fruit!\n\n"..
        "The fruits only glow for a short time before they go bad.\n"..
        "I want to pick them all tonight, so I should avoid any detours.\n"..
        "Can you help me plan my way before it's time to harvest?\n\n"..
        "(Press "..Input:getKeyString("click").." to start)",
    preOutro = "Phew, that was stressful!",
    outro = "Maybe I'm getting too old for this kind of work...",
    homes = {
        {x = 4, y = 4},
    },
    objects = {
        {name = 'fruit', x = 1, y = 2},
        {name = 'fruit', x = 2, y = 2},
        {name = 'fruit', x = 3, y = 2},
        {name = 'fruit', x = 1, y = 3},
        {name = 'fruit', x = 2, y = 3},
        {name = 'fruit', x = 3, y = 3},
    },
    obstacles = {
        {x = 1, y = 2, top = 'wall', right = nil},
        {x = 2, y = 3, top = 'wall', right = 'wall'},
    },
    width = 4,
    height = 4,
    arrows = {
        {
            color = {1,1,1,1},
            count = 7,
        },
    },
    golems = {
        {
            count = 0,
            fixed = {
                {
                    color = {1,1,1,1},
                    x = 3,
                    y = 4,
                    direction = "left",
                    name = "witch",
                    basket = 6,
                    speed = 1,
                },
            },
        },
    },
    stepsLimit = 8,
}

local level = Level:new(name, config)

return level