local ArrowGrid = Class("ArrowGrid")

function ArrowGrid:initialize(config)
    self.config = config
    -- size of the playing field (number of tiles)
    self.width = config.width or 9
    self.height = config.height or 3
    self.arrows = {}

    for x = 1, self.width do
        self.arrows[x] = {}
        for y = 1, self.height do
            self.arrows[x][y] = nil
        end
    end
end

function ArrowGrid:getRandomFreePosition()
    local x = math.random(1, self.width)
    local y = math.random(1, self.height)
    while self.arrows[x][y] do
        x = math.random(1, self.width)
        y = math.random(1, self.height)
    end
    return x, y
end

function ArrowGrid:addArrow(arrow,x,y)
    self.arrows[x][y] = arrow
end

function ArrowGrid:draw(offsetX, offsetY)

    local rx = 1 / 8

    -- draw arrows
    for x = 1, self.width do
        for y = 1, self.height do
            if self.arrows[x][y] then
                self.arrows[x][y]:draw(x, y)
            end
        end
    end
end

return ArrowGrid
