-- a class for grids
-- and some functions for managing them

local ObstaclesGrid = Class("ObstaclesGrid")

function ObstaclesGrid:initialize(config)
    self.config = config
    -- size of the playing field (number of tiles)
    self.width = config.width
    self.height = config.height
    self.color = {0.6, 0.6, 0.6, 1}
    self.tiles = {}

    -- init tiles
    for x = 1, self.width do
        self.tiles[x] = {}
        for y = 1, self.height do
            self.tiles[x][y] = {top = nil, right = nil}
        end
    end
end

function ObstaclesGrid:setObstacle(x, y, top, right)
    if top ~= nil then
        self.tiles[x][y].top = top
    end
    if right ~= nil then
        self.tiles[x][y].right = right
    end
end

function ObstaclesGrid:draw()
    local scale = 1 / Images.stones1:getWidth()

    Daylight:applyReflColor(unpack(self.color))
    for x = 1, self.width do
        for y = 1, self.height do
            local px = x - 1
            local py = y - 1
            local top = self.tiles[x][y].top
            local right = self.tiles[x][y].right

            if top ~= nil then
                love.graphics.draw(Images.stones1, px, py - 0.5, 0, scale, scale)
                --love.graphics.rectangle('fill', px, py - 0.05, 1, 0.1, 0.05)
            end
            if right ~= nil then
                love.graphics.draw(Images.stones2, px + 0.5, py, 0, scale, scale)
                --love.graphics.rectangle('fill', px + 0.95, py, 0.1, 1, 0.05)
            end

        end
    end
end

return ObstaclesGrid
