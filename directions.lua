local Vector = require "lib.hump.vector"

local function createDirection(name, x, y, angle)
    return {
        x = x,
        y = y,
        vec = Vector.new(x, y),
        rotationAngle = angle,
        name = name,
    }
end

UP    = createDirection("up",     0, -1, math.pi * 1.0)
DOWN  = createDirection("down",   0,  1, math.pi * 0.0)
LEFT  = createDirection("left",  -1,  0, math.pi * 0.5)
RIGHT = createDirection("right",  1,  0, math.pi * 1.5)

Directions = {
    UP, RIGHT, DOWN, LEFT
}

DirectionMapping = {
    up    = UP,
    down  = DOWN,
    left  = LEFT,
    right = RIGHT,
}

for i = 1, 4 do
	Directions[i].cw = Directions[i % 4 + 1]
	Directions[i].ccw = Directions[(i + 2) % 4 + 1]
end

DiagonalDirectionMapping = {
    upLeft = {x=-1, y=-1, rotationAngle = math.pi * 0.75},
    upRight = {x=-1, y =1, rotationAngle = math.pi * 1.25},
    downLeft = {x=-1, y=1, rotationAngle = math.pi * 0.25},
    downRight = {x=1, y=1, rotationAngle = math.pi * 1.75},
}
