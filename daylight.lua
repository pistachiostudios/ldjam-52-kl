local DaylightClass = Class("Daylight")

function DaylightClass:initialize()
	self:reset()
end

function DaylightClass:reset()
    self.progress = 0
	self.speed = 0.2
end

function DaylightClass:applyReflColor(r,g,b,a)
	local multiR = math.max(0.5, 1.0 - self.progress * 1.2)
    local multiG = math.max(0.5, 1.2 - self.progress * 1.0)
    local multiB = math.max(0.6, 1.2 - self.progress * 0.3)
    love.graphics.setColor(r * multiR, g * multiG, b * multiB, a)
end

function DaylightClass:applyIllumColor(r,g,b,a)
	local multi = 1.1 + self.progress * 0.4
    love.graphics.setColor(r * multi, g * multi, b * multi, a)
end

function DaylightClass:applyAbstractColor(r,g,b,a)
    love.graphics.setColor(r, g, b, a)
end

function DaylightClass:update(dt)
	if self.progress < 1.0 then
		self.progress = math.min(1.0, self.progress + dt * self.speed)
	end
end

local Daylight = DaylightClass:new()

-- returns a singleton instance!
return Daylight