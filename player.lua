local Basket = require "basket"
local Player = Class("Player")

function Player:initialize(x, y, basketSize, color)
    -- position in grid coordinates
    self.x = x
    self.y = y
    self.direction = "down"
    self.color = color
    self.image = Images.child
    self.homes = {
        {x = x, y = y},
    }
    self.basket = Basket:new(basketSize)
    -- init state of animations etc. here
end

function Player:addHome(x, y)
    -- check if this home is already there
    for i , home in ipairs(self.homes) do
        if home.x == x and home.y == y then
            return
        end
    end
    table.insert(self.homes, {x = x, y = y})
end

function Player:isAtHome()
    for i , home in ipairs(self.homes) do
        if self.x == home.x and self.y == home.y then
            return true
        end
    end
    return false
end

function Player:update(dt)
    -- animation of the player character (like
    -- mouth movement, walking, powerups, etc.) goes here
end

function Player:draw(opacity)
    -- grid-based position
    local posX = self.x - 0.5
    local posY = self.y - 0.5

    local rotation = DirectionMapping[self.direction].rotationAngle

    love.graphics.setColor(self.color[1], self.color[2], self.color[3], opacity)

    -- this are the pixel lenghts of the graphic.
    local width = self.image:getWidth()
    local height = self.image:getHeight()

    local imageSize = math.max(width, height)
    
    -- Using 1/imageSize as scale factor in draw() means that the longer side of the graphic fills the whole tile
    local scale = 1/imageSize
    love.graphics.draw(self.image, posX, posY, rotation, scale, scale, 0.5 * width, 0.5 * height)

end

return Player
