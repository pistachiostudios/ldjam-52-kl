local scene = {}

function scene:draw()
    love.graphics.setColor(1, 1, 1) --white
    love.graphics.draw(Images.background)
    love.graphics.setFont(Fonts.default)
    love.graphics.setColor(0.5, 0.5, 0.5)
    love.graphics.printf("Pistachio Studios presents:", 0, CANVAS_HEIGHT*0.05, CANVAS_WIDTH, "center")
    love.graphics.setFont(Fonts.title)
    love.graphics.setColor(0.1, 0.5, 0.8)
    love.graphics.printf("Grow in the Dark", 0, CANVAS_HEIGHT*0.16, CANVAS_WIDTH, "center")
    love.graphics.setColor(0.5, 0.5, 0.5)
    love.graphics.setFont(Fonts.small)
    love.graphics.printf("made in 72 hours for Ludum Dare 52", 0, CANVAS_HEIGHT*0.7, CANVAS_WIDTH, "center")
    love.graphics.printf("by flauschzelle and lenaschimmel", 0, CANVAS_HEIGHT*0.78, CANVAS_WIDTH, "center")
    love.graphics.setFont(Fonts.default)

    if os.time() % 2 == 0 then
        love.graphics.setColor(1, 1, 1)
        love.graphics.printf("Press "..Input:getKeyString("click").." to start!", 0, CANVAS_HEIGHT*0.87, CANVAS_WIDTH, "center")
    end
end

function scene:handleInput()
    if Input:isPressed("click") then

        -- always go to level 1
        LevelManager:currentLevel():reset()
        LevelManager:currentLevel().started = false
        LevelManager.current = 1

        Roomy:enter(Scenes.gameplay)
    end
    if Input:isPressed("quit") then
        love.window.setFullscreen(false)
        love.event.quit()
    end
end

return scene