local scene = {}

function scene:enter()
    -- limit the player movement speed
    self.playerMovementCooldownMax = 0.15 -- in seconds
    self.playerMovementCooldownRemaining = 0
    self.level = EditorLevels[1]
end

function scene:leave()
end


function scene:update(dt)
    self.level:update(dt)
    if self.playerMovementCooldownRemaining > 0 then
        self.playerMovementCooldownRemaining = self.playerMovementCooldownRemaining - dt
    end
end

function scene:draw()
    love.graphics.setColor(1, 1, 1) --white
    self.level:draw()
end

function scene:handleInput()

    if Input:isPressed("toggleEditor") then
        Roomy:push(Scenes.gameplay)
    end

    if not self.level:isWon() and not self.level:isLost() then
        -- movement controls
        local directionKeys = {"up", "down", "left", "right"}
        for _, dk in ipairs(directionKeys) do
            if Input:isPressed(dk) or Input:isDown(dk) then
                if self.playerMovementCooldownRemaining <= 0 then
                    -- self.level.player.direction = dk
                    -- self.level:movePlayer(DirectionMapping[dk].x, DirectionMapping[dk].y)
                    self.playerMovementCooldownRemaining = self.playerMovementCooldownMax
                end
            end
        end
    end

    if Input:isPressed("quit") then
        love.window.setFullscreen(false)
        love.event.quit()
    end

end

return scene
