local scene = {}

function scene:enter()
    Videos.intro:play()
    Music.intro:play()
end

function scene:leave()
    Music.intro:stop()
end

function scene:draw()
    love.graphics.draw(Videos.intro, 0, 0)
end

function scene:update()
    if not Videos.intro:isPlaying() then
        Roomy:enter(Scenes.title)
    end
end

function scene:handleInput()
    if Input:isPressed("click") then
        Roomy:enter(Scenes.title)
    end
end

return scene
