local scene = {}

local MenuMarginY = 60
local MenuMarginX = 240
local LINES = 5

local BackgroundColor = {0, 0, 0} -- currently unused, but maybe useful later?
local TextColor = {1, 1, 1} -- white
local HighlightTextColor = {0.1, 0.5, 0.8} -- blue
local CursorColor = {0.1, 0.5, 0.8} -- set this to white if you want to use an image as menu cursor!

-- some geometric shapes to use as cursors: --

-- choose which one to use, and the rest can safely be deleted.
-- The example here uses the arrow.

Circle = {} --initialize geometry object with draw function
function Circle:draw(x, y)
    love.graphics.setColor(CursorColor)
    love.graphics.circle("line", x, y, 20)
    love.graphics.setColor(TextColor)
end

Box = {} --initialize geometry object with draw function
function Box:draw(x, y)
    love.graphics.setColor(CursorColor)
    love.graphics.rectangle("line", x, y, 70, 50)
    love.graphics.setColor(TextColor)
end

local arrow = {} --initialize geometry object with draw function
function arrow:draw(x, y)
    local size = 30
    local vertices = {x-size, y, x, y+size, x-size, y+(2*size)}
    love.graphics.setColor(CursorColor)
    love.graphics.polygon("fill", vertices)
    love.graphics.setColor(TextColor)
end

-- end of cursor templates list --

function scene:draw_level_list()
    local lvls = LevelManager.levelCount
    local colWidth = (CANVAS_WIDTH - 2*MenuMarginX)/(math.ceil(lvls/LINES))
    local lineHeight = (CANVAS_HEIGHT - 6*MenuMarginY)/LINES
    local i = 1
    while i <= lvls do
        local number = i..". "
        if i <= 9 then
            number = "0"..i..". "
        end
        local name = number..Levels[i].name
        local posX = MenuMarginX + ((math.ceil(i/LINES)-1)*colWidth)
        local posY = 5*MenuMarginY + (((i-1)%LINES) * lineHeight)


        love.graphics.setColor(1,1,1)

        love.graphics.printf(name, posX, posY, colWidth, "left")
        -- draw selection indicator:
        if i == self.selected then
            arrow:draw(posX - 30, posY - 10)
        end
        i = i +1
    end
end

function scene:draw()
    love.graphics.setColor(HighlightTextColor)
    love.graphics.setFont(Fonts.default)
    love.graphics.printf("Grow In The Dark", 0, MenuMarginY, CANVAS_WIDTH, "center")

    love.graphics.setFont(Fonts.small)
    love.graphics.setColor(TextColor) --white
    self:draw_level_list()

    love.graphics.setFont(Fonts.tiny)
    love.graphics.setColor(HighlightTextColor)
    love.graphics.printf("Press "..string.upper(Input:getKeyString("menu")).." to return to the game or choose a level and press "..Input:getKeyString("click").." to switch levels.", 0, CANVAS_HEIGHT - 180, CANVAS_WIDTH, "center")
    love.graphics.setColor(TextColor) --white
    love.graphics.printf("Edit '" .. ConfigFilePath .. "' to configure the input mapping.", 0, CANVAS_HEIGHT-90, CANVAS_WIDTH, "center")

end

function scene:update(dt)
    --nothing to update here
end

function scene:handleInput()
    if Input:isPressed("quit") then
        Roomy:enter(Scenes.title)
    end

    if Input:isPressed("menu") then
        Roomy:pop() -- leave menu
    end

    if Input:isPressed("click") then
        local newlevelno = self.selected
        if newlevelno <= LevelManager.levelCount then
            -- reset current level state
            if LevelManager.current ~= newlevelno then
                LevelManager:currentLevel():reset()
                if LevelManager:currentLevel().intro ~= nil then
                    LevelManager:currentLevel().started = false
                end
                -- switch to new level
                LevelManager.current = newlevelno
                LevelManager:currentLevel():reset() --reset level state
            end
            -- leave menu
            Roomy:pop()
        end
    end

    if Input:isPressed("up") and self.selected > 1 then
        self.selected = self.selected - 1
    end
    if Input:isPressed("down") and self.selected < LevelManager.levelCount then
        self.selected = self.selected + 1
    end
end

function scene:enter()
    self.selected = LevelManager.current
end

function scene:leave()
    --nothing to do here
end

return scene