-- a class for level editing, derived from 'Level' class

Grid = require "grid"
TracksGrid = require "tracks_grid"
ObstaclesGrid = require "obstacles_grid"
GlowFruit = require "glowfruit"
Golem = require "golem"
Arrow = require "arrow"
ArrowGrid = require "arrowgrid"
GridCursor = require "gridcursor"
Ui = require "ui"
require "lib.helpers"

local LevelEditor = Class("LevelEditor")

local editorColors = {
    {1,1,1,1},
    {1,0,0,1},
    {0,1,0,1},
    {0,0,1,1},
    {1,1,0,1},
    {0,1,1,1},
    {1,0,1,1},
}

function LevelEditor:initialize(name, config)
    self.editMode = true

    self.name = name
    self.config = config
    self.contentText = self.config.contentText

    self.started = true
    self.won = false
    self.lost = false

    -- default grid size
    self.config.width = self.config.width or 4
    self.config.height = self.config.height or 4

    local gameWidthInTiles = self.config.width
    local gameHeightInTiles = self.config.height

    self.TileSize = math.min(
        (CANVAS_WIDTH - (2 * MarginSize))/gameWidthInTiles,
        (CANVAS_HEIGHT - (2 * MarginSize))/gameHeightInTiles
    )

    -- init grids
    self.grid = Grid:new(self.config)
    self.arrowGrid = ArrowGrid:new(self.config)

    -- init fixed objects
    self.golems = {}

    for _, arrowGroupConfig in ipairs(self.config.arrows) do
        if arrowGroupConfig.fixed then
            for _, arrowConfig in ipairs(arrowGroupConfig.fixed) do
                local arrow = Arrow:new(DirectionMapping[arrowConfig.direction], arrowGroupConfig.color)
                arrow.fixed = true
                self.arrowGrid:addArrow(arrow, arrowConfig.x, arrowConfig.y)
            end
        end
    end
    for _, golemGroupConfig in ipairs(self.config.golems) do
        if golemGroupConfig.fixed then
            for _, golemConfig in ipairs(golemGroupConfig.fixed) do
                local golem = Golem:new(self, golemConfig)
                golem.fixed = true
                table.insert(self.golems, golem)
            end
        end
    end

    self.obstaclesGrid = ObstaclesGrid:new(self.config)
    if self.config.obstacles then
        for _, obstacle in ipairs(self.config.obstacles) do
            self.obstaclesGrid:setObstacle(obstacle.x, obstacle.y, obstacle.top, obstacle.right)
        end
    else
        self.obstaclesGrid:setObstacle(1, 2, 'wall', nil)
        self.obstaclesGrid:setObstacle(2, 3, 'wall', 'wall')
    end

    -- init transform for drawing things later
    local gridOffsetX = MarginSize
    local gridOffsetY = MarginSize
    self.gridTransform = love.math.newTransform()
    self.gridTransform:translate(gridOffsetX, gridOffsetY)
    self.gridTransform:scale(self.TileSize, self.TileSize)
    self.fontTransform = love.math.newTransform()
    self.fontTransform:scale(1/self.TileSize, 1/self.TileSize)

    -- init empty grid of collectible objects
    self.fruits = {}
    for x = 1, self.grid.width do
        self.fruits[x] = {}
        for y = 1, self.grid.height do
            self.fruits[x][y] = false
        end
    end

    self.gridCursor = GridCursor:new()

    self:initUi()

    self:reset(true)

end

-- config can be an arrorConfig or golemConfig
function LevelEditor:buttonText(config)
    if self.editMode then
        return " " -- TODO maybe output somethink useful like: fixed count, unfixed count
    else
        return config.remaining .. "/" .. config.count
    end
end

function LevelEditor:initUi()
    self.buildDirection = UP

    self.ui = Ui:new()
    self.placementButtons = { }
    if not self.editMode then
        for _, arrowConfig in ipairs(self.config.arrows) do
            arrowConfig.remaining = arrowConfig.count
            --if arrowConfig.count > 0 then
                arrowConfig.button = Button:new(0, 0, self:buttonText(arrowConfig), Images.arrow, arrowConfig.color, UP.rotationAngle)
                arrowConfig.button.arrowConfig = arrowConfig
                table.insert(self.placementButtons, arrowConfig.button)
            --end
        end
        -- no placable golems in player mode
        -- for _, golemConfig in ipairs(self.config.golems) do
        --     golemConfig.remaining = golemConfig.count
        --     if golemConfig.count > 0 then
        --         golemConfig.button = Button:new(0, 0, self:buttonText(golemConfig), Images.child, golemConfig.color, UP.rotationAngle)
        --         golemConfig.button.golemConfig = golemConfig
        --         table.insert(self.placementButtons, golemConfig.button)
        --     end
        -- end
    else
        for _, color in pairs(editorColors) do
            local arrowConfig = { 
                remaining = 0, -- value does not matter in editor, but must exist
                count = 0, -- value does not matter in editor, but must exist
                color = color,
                fixed = {},
            }
            arrowConfig.button = Button:new(0, 0, self:buttonText(arrowConfig), Images.arrow, arrowConfig.color, UP.rotationAngle)
            arrowConfig.button.arrowConfig = arrowConfig
            table.insert(self.placementButtons, arrowConfig.button)
        end
        for _, color in pairs(editorColors) do
            local golemConfig = { 
                remaining = 0, -- value does not matter in editor, but must exist
                count = 0, -- value does not matter in editor, but must exist
                color = color,
                fixed = {},
            }
            golemConfig.button = Button:new(0, 0, self:buttonText(golemConfig), Images.child, golemConfig.color, UP.rotationAngle)
            golemConfig.button.golemConfig = golemConfig
            table.insert(self.placementButtons, golemConfig.button)
        end
    end

    -- remove button
    self.removeButton = Button:new(0, 0, "remove", Images.remove, {1,0,0,1})
    self.removeButton.onClick = function() self:updateGridCursor() end
    table.insert(self.placementButtons, self.removeButton)

    -- fix/unfix button (part of the level editor)
    if self.editMode then
        self.fixButton = Button:new(0, 0, "fix / unfix", Images.lock_big, {1,1,1,1})
        self.fixButton.onClick = function() self:updateGridCursor() end
        table.insert(self.placementButtons, self.fixButton)

        self.fruitButton = Button:new(0, 0, "fruit", Images.glowfruit, {1,1,1,1})
        self.fruitButton.onClick = function() self:updateGridCursor() end
        table.insert(self.placementButtons, self.fruitButton)

        self.homeButton = Button:new(0, 0, "home", Images.home, {1,1,1,1})
        self.homeButton.onClick = function() self:updateGridCursor() end
        table.insert(self.placementButtons, self.homeButton)
    end

    self.ui:addSelectButtonGroup(self.placementButtons, function() self:updateGridCursor() end)

    self.ui:newRow()
    self.ui:newRow()

    local ccwButton = Button:new(0, 0, "rotate CCW", Images.ccw)
    ccwButton.onClick = function() self:rotateCCW() end
    self.ui:addButton(ccwButton)

    local cwButton = Button:new(0, 0, "rotate CW", Images.cw)
    cwButton.onClick= function() self:rotateCW() end
    self.ui:addButton(cwButton)

    self.ui:newRow()
    self.ui:newRow()

    local startButton = Button:new(0, 0, "Start golems", Images.child)
    startButton.onClick= function()
        for _, golem in pairs(self.golems) do
            golem:start()
        end
        self.harvestStarted = true -- TODO deactivate many actions when started
    end
    self.ui:addButton(startButton)

    self.ui:newRow()

    if self.editMode then
        self.ui:newRow()
        local saveButton = Button:new(0, 0, "Save level", Images.lock_big)
        saveButton.onClick= function()
            self:save()
        end
        self.ui:addButton(saveButton)
        self.ui:newRow()
        self.ui:newRow()
    end

    local softResetButton = Button:new(0, 0, "soft reset", Images.ccw)
    softResetButton.onClick= function()
        self:reset(false)
    end
    self.ui:addButton(softResetButton)

    local hardResetButton = Button:new(0, 0, "HARD RESET", Images.remove, {1,0,0,1})
    hardResetButton.onClick= function()
        self:reset(true)
    end
    self.ui:addButton(hardResetButton)

    local toogleEditModeButton = Button:new(0, 0, "toogle edit mode", Images.roomba_s, {1,0,0,1})
    toogleEditModeButton.onClick= function()
        self.editMode = not self.editMode
        if not self.editMode then
            self.config.arrows = self:getArrowGroups(true)
            self.config.golems = self:getGolemGroups(true)
        end
        self:reset(false)
        self:initUi()
    end
    self.ui:addButton(toogleEditModeButton)


    self.ui:newRow()

end

function LevelEditor:rotateCW()
    self.buildDirection = self.buildDirection.cw
    for _, button in ipairs(self.placementButtons) do
        button.iconRotation = self.buildDirection.rotationAngle
    end
    self:updateGridCursor()
end

function LevelEditor:rotateCCW()
    self.buildDirection = self.buildDirection.ccw
    for _, button in ipairs(self.placementButtons) do
        button.iconRotation = self.buildDirection.rotationAngle
    end
    self:updateGridCursor()
end

function LevelEditor:updateGridCursor()
    for _, button in pairs(self.placementButtons) do
        if button.isSelected then
            if button.golemConfig then
                self.gridCursor:setSecondaryImage(Images.child, self.buildDirection.rotationAngle, button.golemConfig.color)
            elseif button == self.removeButton then
                self.gridCursor:setSecondaryImage(Images.remove, 0, {1,0.5,0.5,1})
            elseif button == self.fixButton then
                self.gridCursor:setSecondaryImage(Images.lock, 0, {1,0.5,0.5,1})
            elseif button == self.homeButton then
                self.gridCursor:setSecondaryImage(Images.home, 0, {1,0.5,0.5,1})
            elseif button == self.fruitButton then
                self.gridCursor:setSecondaryImage(Images.glowfruit, 0, {1,0.5,0.5,1})
            elseif button.arrowConfig then
                self.gridCursor:setSecondaryImage(Images.arrow, self.buildDirection.rotationAngle, button.arrowConfig.color)
            end
        end
    end
end


function LevelEditor:placeFruit(x, y)
    self.fruits[x][y] = GlowFruit:new(self.config.stepsLimit)
end

function LevelEditor:countFruit()
    local object_count = 0
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.fruits[x][y] then
                object_count = object_count + 1
            end
        end
    end
    return object_count
end

function LevelEditor:update(dt)
    -- update glowfruit animations
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.fruits[x][y] then
                self.fruits[x][y]:update(dt)
            end
        end
    end

    -- check win condition: all objects picked and golem baskets empty
    local basketsEmpty = true
    for _, golem in ipairs(self.golems) do
        if golem.basket.itemsCount > 0 then
            basketsEmpty = false
            break
        end
    end
    if self.remaining_fruits == 0 and basketsEmpty then
        self.won = true
    end

    -- check lost condition: steps limit exceeded and not all objects picked
    if self.stepsWalked > self.config.stepsLimit and self.remaining_fruits > 0 then
        self.lost = true
    end

    for _, golem in pairs(self.golems) do
        golem:update(dt)
    end

    self.gridCursor:update(self.gridTransform, self.grid, dt)
    if Input:isPressed("click") and self.gridCursor.inGrid then
        if self.removeButton.isSelected then
            self:removeObject(self.gridCursor.gridX, self.gridCursor.gridY)
        elseif self.editMode and self.fixButton.isSelected then
            self:fixOrUnfixObject(self.gridCursor.gridX, self.gridCursor.gridY)
        else
            self:addObject(self.gridCursor.gridX, self.gridCursor.gridY)
        end
    end

    if Input:isPressed("rotateCW") then
        self:rotateCW()
    end

    if Input:isPressed("rotateCCW") then
        self:rotateCCW()
    end

    self.ui:update()
end

function LevelEditor:addObject(x, y)
    for _, button in pairs(self.placementButtons) do
        if button.isSelected then
            if button.golemConfig then
                if button.golemConfig.remaining > 0 or self.editMode then
                    table.insert(self.golems, Golem:new(self, {x = x, y = y, direction = self.buildDirection, basket = button.golemConfig.basketSize, color = button.golemConfig.color}))
                    button.golemConfig.remaining = button.golemConfig.remaining - 1
                    button.text = self:buttonText(button.golemConfig)
                end
            elseif button.arrowConfig then
                if button.arrowConfig.remaining > 0 or self.editMode then
                    self.arrowGrid:addArrow(Arrow:new(self.buildDirection, button.arrowConfig.color), x, y)
                    button.arrowConfig.remaining = button.arrowConfig.remaining - 1
                    button.text = self:buttonText(button.arrowConfig)
                end
            elseif button == self.homeButton then
                for i , home in ipairs(self.homes) do
                    if x == home.x and y == home.y then
                        return
                    end
                end
                table.insert(self.homes, {x = x, y = y})
            elseif button == self.fruitButton then
                if not self.fruits[x][y] then
                    self:placeFruit(x, y)
                    self.remaining_fruits = self:countFruit()
                end
            end
        end
    end
end

function LevelEditor:removeObject(x, y)
    for i, golem in pairs(self.golems) do
        if golem.pos.x == x and golem.pos.y == y and (not golem.fixed or self.editMode) then
            table.remove(self.golems, i)
            if not self.editMode then
                for _, golemConfig in pairs(self.config.golems) do
                    if golemConfig.color == golem.color then
                        golemConfig.remaining = golemConfig.remaining + 1
                        if golemConfig.button then
                            golemConfig.button.text  =self:buttonText(golemConfig)
                        end
                        return
                    end
                end
            end
        end
    end
    if self.arrowGrid.arrows[x][y] then
        local arrow = self.arrowGrid.arrows[x][y]
        if not arrow.fixed or self.editMode then
            self.arrowGrid.arrows[x][y] = nil
            if not self.editMode then
                for _, arrowConfig in pairs(self.config.arrows) do
                    if arrowConfig.color == arrow.color then
                        arrowConfig.remaining = arrowConfig.remaining + 1
                        if arrowConfig.button then
                            arrowConfig.button.text = self:buttonText(arrowConfig)
                        end
                        return
                    end
                end
            end
        end
    end
    if self.fruits[x][y] and self.editMode then
        self.fruits[x][y] = false
        return
    end
    for i , home in ipairs(self.homes) do
        if x == home.x and y == home.y and self.editMode then
            table.remove(self.homes, i)
            return
        end
    end
end

function LevelEditor:fixOrUnfixObject(x, y)
    for i, golem in pairs(self.golems) do
        if golem.pos.x == x and golem.pos.y == y then
            golem.fixed = not golem.fixed
            return
        end
    end
    if self.arrowGrid.arrows[x][y] then
        local arrow = self.arrowGrid.arrows[x][y]
        arrow.fixed = not arrow.fixed
        return
    end
end


-- TODO call this somewhere
function LevelEditor:nextStep()
    -- increment steps counter
    self.stepsWalked = self.stepsWalked + 1
    -- decrement fruits' health
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.fruits[x][y] then
                self.fruits[x][y]:decrementHealth()
            end
        end
    end
end

function LevelEditor:draw()
    -- for compatibility with old draw functions that still use TileSize
    TileSize = 1
    
    -- apply a transform that accounts for TileSize and offsets
    love.graphics.push()
    love.graphics.applyTransform(self.gridTransform)
    
    -- draw the actual level content here:
    self.grid:draw()
    self.arrowGrid:draw()
    self.obstaclesGrid:draw()
    for _, golem in pairs(self.golems) do
        golem.tracks_grid:draw()
    end

    -- draw homes
    Daylight:applyReflColor(0.5, 0.25, 0, 1)
    for _ , home in ipairs(self.homes) do
        love.graphics.rectangle('fill', home.x - 0.875, home.y - 0.875, 0.75, 0.75)
    end

    -- draw objects (glowfruits)
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.fruits[x][y] then
                self.fruits[x][y]:draw(x, y)
            end
        end
    end

    for _, golem in pairs(self.golems) do
        golem:draw()
    end

    self.gridCursor:draw()

    -- revert the transform because the following is not grid-based
    love.graphics.pop()

    self.ui:draw()

    -- at last, draw text overlays for some special states:
    -- love.graphics.setFont(Fonts.default)
    -- love.graphics.setColor(1,1,1)

end

function LevelEditor:start()
    self.started = true
end

function LevelEditor:isWon()
    return self.won
end

function LevelEditor:isLost()
    return self.lost
end

function LevelEditor:reset(isHardReset)
    -- softReset: restore state before the harvest was started
    -- hardReset: undo everything the player has done

    self.harvestStarted = false
    for i, golem in ipairs(self.golems) do
        golem:reset()
    end

    -- default value for homes
    if not self.config.homes then
        self.config.homes = {{x=4,y=4}}
    end

    -- default basket size
    if not self.config.basket then
        self.config.basket = 6
    end

    if isHardReset then
        for i = #self.golems, 1, -1 do
            if not self.golems[i].fixed then
                table.remove(self.golems, i)
            end
        end

        for x = 1, self.arrowGrid.width do
            for y = 1, self.arrowGrid.height do
                local arrow = self.arrowGrid.arrows[x][y]
                if arrow and not arrow.fixed then
                    --self.arrowGrid.arrows[x][y] = nil
                    self:removeObject(x,y)
                end
            end
        end
    end

    self.homes = self.config.homes

    if self.config.objects then
        for _, object in ipairs(self.config.objects) do
            self:placeFruit(object.x, object.y)
        end
    else
        -- default objects
        self:placeFruit(1, 2)
        self:placeFruit(1, 3)
        self:placeFruit(2, 2)
        self:placeFruit(2, 3)
        self:placeFruit(3, 2)
        self:placeFruit(3, 3)
    end

    self.remaining_fruits = self:countFruit()

    self.won = false
    self.lost = false
    -- reset steps
    self.config.stepsLimit = self.config.stepsLimit or 12
    self.stepsWalked = 0

    -- not setting 'started' to false here because this is also
    -- used for restarting and the intro should not be shown again then
end

function LevelEditor:getArrowGroups(skipCount)
    local arrowGroups = {}
    
    for x = 1, self.arrowGrid.width do
        for y = 1, self.arrowGrid.height do
            if self.arrowGrid.arrows[x][y] then
                local arrow = self.arrowGrid.arrows[x][y]
                local group = { -- new group, might be discarded if one with the same color already exists
                    color = arrow.color,
                    count = 0,
                    fixed = {},
                    isNew = true,
                }
                for _, existingGroup in pairs(arrowGroups) do
                    if sameColors(existingGroup.color, arrow.color) then
                        group = existingGroup
                        break
                    end
                end
                if arrow.fixed then
                    arrow.x = x
                    arrow.y = y
                    table.insert(group.fixed, arrow)
                elseif not skipCount then
                    group.count = group.count + 1
                end
                if group.isNew then
                    group.isNew = false
                    table.insert(arrowGroups, group)
                end
            end
        end
    end
    return arrowGroups
end

function LevelEditor:getGolemGroups(skipCount)
    -- the concept of golems changed last-minute, so there are no groups in theory
    -- let's just say there is always ONE group instead
    local group = {
        count = 0,
        fixed = {},
    }
    local golemGroups = { group }
    
    for _, golem in ipairs(self.golems) do
        if golem.fixed then
            table.insert(group.fixed, golem)
        elseif not skipCount then
            group.count = group.count + 1
        end
    end
    return golemGroups
end

function LevelEditor:save()

    -- homes
    print("    homes = {")
    for i , home in ipairs(self.homes) do
        print("        {x = "..home.x..", y = "..home.y.."},")
           
    end
    print("    },")

    -- objects / fruits
    print("    objects = {")
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.fruits[x][y] then
                print("        {name = 'fruit', x = "..x..", y = "..y.."},")
            end
        end
    end
    print("    },")
    
    -- arrows
    -- first, collect them into groups by color
    local arrowGroups = self:getArrowGroups()
    -- then output the groups
    print("    arrows = {")
    for _, g in pairs(arrowGroups) do
        print("        {")
        print("            color = {"..g.color[1]..","..g.color[2]..","..g.color[3]..","..g.color[4].."},")
        print("            count = "..g.count..",")
        if #g.fixed > 0 then
            print("            fixed = {")
            for _, a in ipairs(g.fixed) do     
                print("                {")
                print("                    x = "..a.x..",")
                print("                    y = "..a.y..",")
                print("                    direction = '"..a.direction.name.."',")
                print("                }")
            end
            print("            },")
        end
        print("        },")
    end

    print("    },")
    
    -- golems
    -- first, collect them into groups by color
    local golemGroups = self:getGolemGroups()
    -- then output the groups
    print("    golems = {")
    for _, g in pairs(golemGroups) do
        print("        {")
        print("            count = "..g.count..",")
        if #g.fixed > 0 then
            print("            fixed = {")
            for _, golem in ipairs(g.fixed) do     
                print("                {")
                print("                    color = {"..golem.color[1]..","..golem.color[2]..","..golem.color[3]..","..golem.color[4].."},")
                print("                    x = "..golem.pos.x..",")
                print("                    y = "..golem.pos.y..",")
                print("                    direction = '"..golem.direction.name.."',")
                if golem.basket.capacity then
                    print("                    basketSize = "..golem.basket.capacity..",")
                end
                if golem.name then
                    print("                    name = '"..golem.name.."',")
                end
                print("                },")
            end
            print("            },")
        end
        print("        },")
    end

    print("    },")
end

return LevelEditor