local ArrowButton = Class("ArrowButton")

function ArrowButton:initialize(x, y, text, icon, iconColor, iconRotation, isToggleButton)
	self.text = text
	self.x = x
	self.y = y
	self.font = Fonts.small
	
	self.width = 120
	self.height = 120
	
	if icon then
		self.icon = icon
		self.iconColor = iconColor or {1,1,1,1}
		self.iconRotation = iconRotation or 0
		local pixelWidth = self.icon:getWidth()
		local pixelHeight = self.icon:getHeight()
		self.iconHeight = 100
		self.iconScale = self.iconHeight / pixelHeight
		self.iconWidth = self.iconHeight / pixelHeight * pixelWidth
	end

	self.isToggleButton = isToggleButton
	self.isHovered = false  -- as long as mouse is inside rect
	self.isClicked = false  -- as long as mouse is down and inside rect
	self.isSelected = false -- permanent, if this is a toggle button

	self.onClick = nil
	self.onSelect = nil
	self.onDeselect = nil
end

function ArrowButton:update()
	local mouseX, mouseY = love.mouse.getPosition()
	self.isHovered = mouseX >= self.x and
				     mouseY >= self.y and
				     mouseX <= self.x + self.width and
				     mouseY <= self.y + self.height

	-- logical click, occurs only once
	local isNewClicked = Input:isPressed("click") and self.isHovered
	-- graphical click, lasts as long as mouse is down
	self.isClicked = Input:isDown("click") and self.isHovered
	
	if isNewClicked then
		if self.onClick then
			self.onClick(self)
		end

		if self.isToggleButton then
			if self.isSelected then
				self.isSelected = false
				if self.onDeselect then
					self.onDeselect(self)
				end
			else
				self.isSelected = true
				if self.onSelect then
					self.onSelect(self)
				end
			end
		end
	end
end

function ArrowButton:draw()
	if self.isClicked then
		love.graphics.setColor(0.7,0.7,0.7,1)
	elseif self.isHovered then
		love.graphics.setColor(0.5,0.5,0.5,1)
	else
		love.graphics.setColor(0.3,0.3,0.3,1)
	end
	love.graphics.setFont(self.font)
	love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)

	love.graphics.setColor(unpack(self.iconColor))
	love.graphics.draw(self.icon, self.x + 10 + 0.5 * self.iconWidth, self.y + 10 + 0.5 * self.iconHeight, self.iconRotation, self.iconScale, self.iconScale, 0.5 * self.icon:getWidth(), 0.5 * self.icon:getHeight())
	love.graphics.setColor(1,1,1,1)
	love.graphics.printf(self.text, self.x - 40, self.y + self.height + 10, self.width + 80, "center")


	if self.isSelected then
		love.graphics.setColor(1,1,1,1)
		love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
		love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
	end
end

return ArrowButton
