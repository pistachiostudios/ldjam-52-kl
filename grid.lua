-- a class for grids
-- and some functions for managing them

local Grid = Class("Grid")

function Grid:initialize(config)
    self.config = config
    -- size of the playing field (number of tiles)
    self.width = config.width or 9
    self.height = config.height or 3
    self.config.tileColor = config.tileColor or {0.2, 0.2, 0.2, 1.0}
    self.config.wallColor = config.wallColor or {0.5, 0.5, 0.5, 1.0}
    self.tiles = {}

    for x = 1, self.width do
        self.tiles[x] = {}
        for y = 1, self.height do
            self.tiles[x][y] = math.random(12)
        end
    end

end

function Grid:containsPoint(x, y)
    return not (x < 1 or x > self.width or y < 1 or y > self.height)
end

function Grid:isWalkable(x, y)
    if not self:containsPoint(x, y) then
        return false
    end
    return self.tiles[x][y] > 0
end

function Grid:getRandomFreePosition()
    local x = math.random(1, self.width)
    local y = math.random(1, self.height)
    while not self:isWalkable(x, y) do
        x = math.random(1, self.width)
        y = math.random(1, self.height)
    end
    return x, y
end

function Grid:draw()

    --local rx = 1 / 8

    -- draw tiles
    Daylight:applyReflColor(1, 1, 1, 1)
    for x = 1, self.width do
        for y = 1, self.height do
            local imagename = "ground"..self.tiles[x][y]
            local scale = 1 / Images.ground1:getHeight()
            love.graphics.draw(Images[imagename], x-1, y-1, 0, scale, scale)
            --love.graphics.rectangle("fill", x-1, y-1, 1, 1, rx)
        end
    end

end

return Grid
