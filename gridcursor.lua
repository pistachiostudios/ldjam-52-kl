local GridCursor = Class("GridCursor")

function GridCursor:initialize()
	self.frameImage = Images.frame
	local width = self.frameImage:getWidth()
	local height = self.frameImage:getHeight()
	local imageSize = math.max(width, height)
	self.scale = 1/imageSize

	self.inGrid = false
	self.time = 0
end

function GridCursor:setSecondaryImage(image, rotation, color)
	self.secondaryImage = image
	if image then
		local width = self.secondaryImage:getWidth()
		local height = self.secondaryImage:getHeight()
		local imageSize = math.max(width, height)
		self.secondaryImageScale = 1/imageSize
		color = color or {1,1,1,1}
		self.secondaryImageColor = { color[1], color[2], color[3], color[4] * 0.5 }
		self.secondaryImageRoatation = rotation
	end
end

function GridCursor:update(gridTransform, grid, dt)
	self.time = self.time + dt
	local mouseX, mouseY = love.mouse.getPosition()
    local gridX, gridY = gridTransform:inverseTransformPoint(mouseX, mouseY)
    self.gridX = math.floor(gridX + 1)
    self.gridY = math.floor(gridY + 1)
	self.inGrid = grid:containsPoint(self.gridX, self.gridY)
end

function GridCursor:draw()
	if self.inGrid then
		Daylight:applyAbstractColor(1,1,1,0.3)
    	love.graphics.draw(self.frameImage, self.gridX - 1, self.gridY - 1, 0, self.scale, self.scale)
		if self.secondaryImage then
			local width = self.secondaryImage:getWidth()
    		local height = self.secondaryImage:getHeight()
			local currentScale = 0.7 + math.sin(self.time) * 0.2
			Daylight:applyAbstractColor(unpack(self.secondaryImageColor))
    		love.graphics.draw(self.secondaryImage, self.gridX - 0.5, self.gridY - 0.5, self.secondaryImageRoatation, self.secondaryImageScale * currentScale, self.secondaryImageScale * currentScale, 0.5 * width, 0.5 * height)
		end
	end
end

return GridCursor
