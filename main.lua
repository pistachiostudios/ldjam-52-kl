require "lib.slam"
require "lib.helpers"
require "directions"
local moonshine = require 'lib.moonshine'

Vector = require "lib.hump.vector"
Tlfres = require "lib.tlfres"
Class = require 'lib.middleclass' -- see https://github.com/kikito/middleclass
LibRoomy = require 'lib.roomy' -- see https://github.com/tesselode/roomy
LibInput = require "lib.input.Input"
Level = require "level"
LevelEditor = require "level_editor"
Inspect = require('lib.inspect') -- useful as global require for debugging
Daylight = require "daylight"

CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080

-- for grid-based games:
MarginSize = CANVAS_HEIGHT/20

Roomy = LibRoomy.new() -- roomy is the manager for all the rooms (scenes) in our game
Scenes = {} -- initialize list of scenes

Input = LibInput:new()

HintBasketFull = 'My basket is full!\n I have to go home and empty it before I can pick more fruit.\n\n'..
    "(Press "..Input:getKeyString("click").." to continue)"
ShownHintBasketFull = false

function love.load(args)
    -- initialize randomness in two ways:
    love.math.setRandomSeed(os.time())
    math.randomseed(os.time())

    -- set up default drawing options
    love.graphics.setBackgroundColor(0, 0, 0)

    -- load assets
    Images = {}
    for file in listUsefulFiles("images") do
        Images[file.basename] = love.graphics.newImage(file.path)
    end

    Sounds = {}
    for file in listUsefulFiles("sounds") do
        Sounds[file.basename] = love.audio.newSource(file.path, "static")
    end

    Music = {}
    for file in listUsefulFiles("music") do
        Music[file.basename] = love.audio.newSource(file.path, "stream")
        Music[file.basename]:setLooping(true)
    end
    MusicMuted = false

    Videos = {}
    for file in listUsefulFiles("videos") do
        Videos[file.basename] = love.graphics.newVideo(file.path, {audio=false})
    end

    Fonts = {}
    for file in listUsefulFiles("fonts") do
        Fonts[file.basename] = {}
        local sizes = {20, 40, 60, 132}
        for _, fontsize in ipairs(sizes) do
            Fonts[file.basename][fontsize] = love.graphics.newFont(file.path, fontsize)
        end
    end
    Fonts.title = Fonts["HennyPenny-Regular"][132]
    Fonts.default = Fonts["Milonga-Regular"][60]
    Fonts.small = Fonts["Milonga-Regular"][40]
    Fonts.tiny = Fonts["Milonga-Regular"][20]
    love.graphics.setFont(Fonts.default)

    -- levels
    local basenames = {}
    for file in listUsefulFiles("levels") do
        table.insert(basenames, file.basename)
    end
    table.sort(basenames) --sort level filenames in alphabetical order
    for i, level in ipairs(basenames) do
        Levels[i] = require ("levels." .. level)
    end

    -- level editor
    EditorLevels = {}
    local editor_basenames = {}
    for file in listUsefulFiles("editor_levels") do
        table.insert(editor_basenames, file.basename)
    end
    table.sort(editor_basenames) --sort filenames in alphabetical order
    for i, editor_level in ipairs(editor_basenames) do
        EditorLevels[i] = require ("editor_levels." .. editor_level)
    end

    InitLevelManager() -- this must be done after the levels are loaded from the filesystem
    if args[1] and tonumber(args[1]) > 0 then
        LevelManager.current = tonumber(args[1])
    end

    -- scenes
    for file in listUsefulFiles("scenes") do
        Scenes[file.basename] = require ("scenes." .. file.basename)
    end
    Roomy:hook({exclude = {"draw", "update"}}) --hook roomy in to manage the scenes (with exceptions)
    if Videos.intro then
        Roomy:enter(Scenes.intro) -- start with intro video if it exists
    else
        --Roomy:enter(Scenes.gameplay) --FIXME skip title for easier testing
        Roomy:enter(Scenes.title) --start on title screen
    end

    -- set up effects
    Effect = moonshine(moonshine.effects.scanlines).chain(moonshine.effects.colorgradesimple).chain(moonshine.effects.glow).chain(moonshine.effects.godsray)
    Effect.godsray.exposure = 0.2
    Effect.godsray.weight = 0.2
    Effect.godsray.samples = 5
    Effect.godsray.density = 0.02
    --Effect.glow.min_luma = 0.1
    Effect.glow.strength = 20
    Effect.scanlines.width = 2
    Effect.scanlines.thickness = 1
    Effect.scanlines.opacity = 0.15

    -- comment out any of the following lines to enable effects:
    Effect.disable("scanlines")
    Effect.disable("colorgradesimple")
    Effect.disable("glow")
    Effect.disable("godsray")
end

function love.update(dt)
    Input:update(dt)
    HandleInput()
    Roomy:emit("handleInput")
    Roomy:emit("update", dt)
end

function love.mousemoved(x, y, dx, dy, istouch)
    Input:mouseMoved(dx, dy)
end

-- not sure if we need this, only makes sense with absolute mouse positions
function love.mouse.getPosition()
    return Tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

-- helper function for dimming the level behind when text is shown
function DimRect()
    Daylight:applyAbstractColor(0, 0, 0, 0.5)
    love.graphics.rectangle("fill", 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)
    Daylight:applyAbstractColor(1, 1, 1)
end

-- helper function for things that change color
function ColorCycle(i)
    local speed = 0.05
    local TAU = 2*math.pi

    return {
        0.5+0.5*math.cos(i*speed + 0),
        0.5+0.5*math.cos(i*speed + TAU/3),
        0.5+0.5*math.cos(i*speed + TAU*2/3),
    }
end

function HandleInput()
    if Input:isPressed("quit") then
        love.window.setFullscreen(false)
        love.event.quit()
    end
    if Input:isPressed("fullscreen") then
        local isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen, "desktop")
    end
end

function love.draw()
    Daylight:applyAbstractColor(1, 1, 1) -- white

    Effect(function()
        Tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

        -- (draw any global stuff here)
        -- Show this somewhere to the user so they know where to configure
        -- love.graphics.printf("Edit '" .. ConfigFilePath .. "' to configure the input mapping.", 0, 990, CANVAS_WIDTH, "center")

        -- draw scene-specific stuff:
        Roomy:emit("draw")

        Tlfres.endRendering()
    end)
end

function love.resize(w, h)
    Effect.resize(w, h)
end