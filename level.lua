-- a class for levels
-- and some functions for managing them

Grid = require "grid"
TracksGrid = require "tracks_grid"
ObstaclesGrid = require "obstacles_grid"
GlowFruit = require "glowfruit"
Golem = require "golem"
Arrow = require "arrow"
ArrowGrid = require "arrowgrid"
GridCursor = require "gridcursor"
Ui = require "ui"
require "lib.helpers"
ArrowButton = require "arrowbutton"

local Level = Class("Level")

function Level:initialize(name, config)
    self.harvestStarted = false

    self.name = name
    self.config = config
    self.contentText = self.config.contentText

    self.started = false
    self.won = false
    self.lost = false

    -- nil is acceptable as a default value here!
    self.intro = self.config.intro
    -- start immediately if the level has no intro
    if self.intro == nil then
        self:start()
    end

    -- preOutro text, may be nil
    self.preOutro = self.config.preOutro

    -- default outro text
    self.outro = self.config.outro
    if self.outro == nil then
        self.outro = "Congratulations, you won! Click to continue."
    end

    -- default lost text
    self.lostMessage = self.config.lostMessage
    if self.lostMessage == nil then
        self.lostMessage = "Oh no, some of the fruit went bad before I could pick it :(\n\n"..
            "Press "..Input:getKeyString("click").." to restart."
    end

    -- default grid size
    self.config.width = self.config.width or 4
    self.config.height = self.config.height or 4

    local gameWidthInTiles = self.config.width
    local gameHeightInTiles = self.config.height

    self.TileSize = math.min(
        (CANVAS_WIDTH - (2 * MarginSize))/gameWidthInTiles,
        (CANVAS_HEIGHT - (2 * MarginSize))/gameHeightInTiles
    )

    -- init grids
    self.grid = Grid:new(self.config)
    self.arrowGrid = ArrowGrid:new(self.config)

    -- init fixed objects
    self.golems = {}

    for _, arrowGroupConfig in ipairs(self.config.arrows) do
        if arrowGroupConfig.fixed then
            for _, arrowConfig in ipairs(arrowGroupConfig.fixed) do
                local arrow = Arrow:new(DirectionMapping[arrowConfig.direction], arrowGroupConfig.color)
                arrow.fixed = true
                self.arrowGrid:addArrow(arrow, arrowConfig.x, arrowConfig.y)
            end
        end
    end
    for _, golemGroupConfig in ipairs(self.config.golems) do
        if golemGroupConfig.fixed then
            for _, golemConfig in ipairs(golemGroupConfig.fixed) do
                local golem = Golem:new(self, golemConfig)
                golem.fixed = true
                table.insert(self.golems, golem)
            end
        end
    end

    self.obstaclesGrid = ObstaclesGrid:new(self.config)
    if self.config.obstacles then
        for _, obstacle in ipairs(self.config.obstacles) do
            self.obstaclesGrid:setObstacle(obstacle.x, obstacle.y, obstacle.top, obstacle.right)
        end
    else
        self.obstaclesGrid:setObstacle(1, 2, 'wall', nil)
        self.obstaclesGrid:setObstacle(2, 3, 'wall', 'wall')
    end

    -- init transform for drawing things later
    local gridOffsetX = MarginSize
    local gridOffsetY = MarginSize
    self.gridTransform = love.math.newTransform()
    self.gridTransform:translate(gridOffsetX, gridOffsetY)
    self.gridTransform:scale(self.TileSize, self.TileSize)
    self.fontTransform = love.math.newTransform()
    self.fontTransform:scale(1/self.TileSize, 1/self.TileSize)

    -- init empty grid of collectible objects
    self.fruits = {}
    for x = 1, self.grid.width do
        self.fruits[x] = {}
        for y = 1, self.grid.height do
            self.fruits[x][y] = false
        end
    end

    -- template for drawing fruits before harvest starts
    self.unripeFruit = GlowFruit:new(3)
    self.unripeFruit:decrementHealth() -- sets the images

    self.preWonTime = 1 -- duration between first and second outro text, e.g. for playing a sound
    if self.preOutro == nil then
        self.preWonTime = 0
    end
    self.preWonTimer = self.preWonTime

    self.gridCursor = GridCursor:new()

    self:initUi()

    self.stepTimeCounter = 0
    self.stepTime = 1

    self:reset(true)

end

-- config can be an arrowConfig or golemConfig
function Level:buttonText(config)
    return config.remaining .. "/" .. config.count
end

function Level:initUi()
    self.buildDirection = UP

    self.ui = Ui:new()
    self.placementButtons = { }
    local numArrowButtons = #self.config.arrows
    local widthArrowButtons = numArrowButtons * 150 - 30
    local center = 1500
    local left = center - (widthArrowButtons / 2)
    for i, arrowConfig in ipairs(self.config.arrows) do
        arrowConfig.remaining = arrowConfig.count

        arrowConfig.button = ArrowButton:new(left + (i-1)*150, 100, self:buttonText(arrowConfig), Images.arrow, arrowConfig.color, UP.rotationAngle)
        arrowConfig.button.arrowConfig = arrowConfig
        table.insert(self.placementButtons, arrowConfig.button)
        self.ui:addButton(arrowConfig.button, true)
    end

    -- remove button
    self.removeButton = ArrowButton:new(center - 60, 500, "remove", Images.remove, {1,0.8,0.8,1})
    self.removeButton.onClick = function() self:updateGridCursor() end
    table.insert(self.placementButtons, self.removeButton)
    self.ui:addButton(self.removeButton, true)

    self.ui:addSelectButtonGroup(self.placementButtons, function() self:updateGridCursor() end, true)

    self.ui:newRow()
    self.ui:newRow()

    
    -- local ccwButton = Button:new(0, 0, "rotate CCW", Images.ccw)
    -- ccwButton.onClick = function() self:rotateCCW() end
    -- self.ui:addButton(ccwButton)

    -- local cwButton = Button:new(0, 0, "rotate CW", Images.cw)
    -- cwButton.onClick= function() self:rotateCW() end
    -- self.ui:addButton(cwButton)
    

    self.ui:newRow()
    self.ui:newRow()

    local startButton = Button:new(center - 200, 800, "Start Harvest!", nil)
    startButton.onClick= function()
        for _, golem in pairs(self.golems) do
            golem:start()
        end
        self.harvestStarted = true
        Sounds.woosh:play()
    end
    self.ui:addButton(startButton, true)

    self.harvestUi = Ui:new()

    local softResetButton = Button:new(center - 200, 800, "Cancel Harvest", nil)
    softResetButton.onClick= function()
        self:reset(false)
    end
    self.harvestUi:addButton(softResetButton, true)

    -- local hardResetButton = Button:new(0, 0, "HARD RESET", Images.remove, {1,0,0,1})
    -- hardResetButton.onClick= function()
    --     self:reset(true)
    -- end
    -- self.harvestUi:addButton(hardResetButton)
end

function Level:rotateCW()
    self.buildDirection = self.buildDirection.cw
    for _, button in ipairs(self.placementButtons) do
        button.iconRotation = self.buildDirection.rotationAngle
    end
    self:updateGridCursor()
end

function Level:rotateCCW()
    self.buildDirection = self.buildDirection.ccw
    for _, button in ipairs(self.placementButtons) do
        button.iconRotation = self.buildDirection.rotationAngle
    end
    self:updateGridCursor()
end

function Level:updateGridCursor()
    for _, button in pairs(self.placementButtons) do
        if button.isSelected then
            if button.golemConfig then
                self.gridCursor:setSecondaryImage(Images.child, self.buildDirection.rotationAngle, button.golemConfig.color)
            elseif button == self.removeButton then
                self.gridCursor:setSecondaryImage(Images.remove, 0, {1,0.5,0.5,1})
            elseif button == self.fixButton then
                self.gridCursor:setSecondaryImage(Images.lock, 0, {1,0.5,0.5,1})
            elseif button == self.homeButton then
                self.gridCursor:setSecondaryImage(Images.home, 0, {1,0.5,0.5,1})
            elseif button == self.fruitButton then
                self.gridCursor:setSecondaryImage(Images.glowfruit, 0, {1,0.5,0.5,1})
            elseif button.arrowConfig then
                self.gridCursor:setSecondaryImage(Images.arrow, self.buildDirection.rotationAngle, button.arrowConfig.color)
            end
        end
    end
end


function Level:placeFruit(x, y)
    self.fruits[x][y] = GlowFruit:new(self.config.stepsLimit)
    self.plants[x][y] = true
end

function Level:countFruit()
    local object_count = 0
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.fruits[x][y] then
                object_count = object_count + 1
            end
        end
    end
    return object_count
end

function Level:update(dt)
    if not(self.lost or self.won or self.preWon or not self.started) then

        -- update glowfruit animations
        for x = 1, self.grid.width do
            for y = 1, self.grid.height do
                if self.fruits[x][y] then
                    self.fruits[x][y]:update(dt)
                end
            end
        end

        -- check win condition: all objects picked and golem baskets empty
        local basketsEmpty = true
        for _, golem in ipairs(self.golems) do
            if golem.basket.itemsCount > 0 then
                basketsEmpty = false
                break
            end
        end
        if self.remaining_fruits == 0 and basketsEmpty then
            self:win()
        end

        -- check lost condition: steps limit exceeded and not all objects picked
        if self.stepsWalked > self.config.stepsLimit and self.remaining_fruits > 0 then
            Sounds.oh_no:play()
            self.lost = true
        end

        -- update golems
        for _, golem in pairs(self.golems) do
            golem:update(dt)
        end

        if self.harvestStarted then
            -- update steps counter
            self.stepTimeCounter = self.stepTimeCounter + dt
            if self.stepTimeCounter >= self.stepTime then
                self.stepTimeCounter = 0
                self:nextStep()
            end

            -- progress dawn
            Daylight:update(dt)
        else -- not during harvest:

            -- update grid cursor
            self.gridCursor:update(self.gridTransform, self.grid, dt)
            if Input:isPressed("click") and self.gridCursor.inGrid then
                if self.removeButton.isSelected then
                    self:removeObject(self.gridCursor.gridX, self.gridCursor.gridY)
                else
                    self:addObject(self.gridCursor.gridX, self.gridCursor.gridY)
                end
            end
            
            if Input:isPressed("rotateCW") then
                self:rotateCW()
            end

            if Input:isPressed("rotateCCW") then
                self:rotateCCW()
            end
        end

        if self.harvestStarted then
            self.harvestUi:update()
        else
            self.ui:update()
        end

    elseif self.preWon then
        self.preWonTimer = self.preWonTimer - dt
        if self.preWonTimer <= 0 then
            self.won = true
        end
    end
end

function Level:addObject(x, y)
    for _, button in pairs(self.placementButtons) do
        if button.isSelected then
            if button.golemConfig then
                if button.golemConfig.remaining > 0 then
                    table.insert(self.golems, Golem:new(self, {x = x, y = y, direction = self.buildDirection, basket = button.golemConfig.basketSize, color = button.golemConfig.color}))
                    button.golemConfig.remaining = button.golemConfig.remaining - 1
                    button.text = self:buttonText(button.golemConfig)
                end
            elseif button.arrowConfig then
                local oldArrow = self.arrowGrid.arrows[x][y]
                if oldArrow then
                    -- remove old arrow if the player already has a new arrow to place, or if arrows have the same color
                    if button.arrowConfig.remaining > 0 or sameColors(button.arrowConfig.color, oldArrow.color) then
                        self:removeObject(x, y)
                    end
                end
                if button.arrowConfig.remaining > 0 then
                    self.arrowGrid:addArrow(Arrow:new(self.buildDirection, button.arrowConfig.color), x, y)
                    button.arrowConfig.remaining = button.arrowConfig.remaining - 1
                    button.text = self:buttonText(button.arrowConfig)
                end
                Sounds.place_arrow:setVolume(0.1)
                Sounds.place_arrow:play()
            elseif button == self.homeButton then
                for i , home in ipairs(self.homes) do
                    if x == home.x and y == home.y then
                        return
                    end
                end
                table.insert(self.homes, {x = x, y = y})
            elseif button == self.fruitButton then
                if not self.fruits[x][y] then
                    self:placeFruit(x, y)
                    self.remaining_fruits = self:countFruit()
                end
            end
        end
    end
end

function Level:removeObject(x, y)
    if self.arrowGrid.arrows[x][y] then
        local arrow = self.arrowGrid.arrows[x][y]
        if not arrow.fixed then
            self.arrowGrid.arrows[x][y] = nil
            for _, arrowConfig in pairs(self.config.arrows) do
                if arrowConfig.color == arrow.color then
                    arrowConfig.remaining = arrowConfig.remaining + 1
                    if arrowConfig.button then
                        arrowConfig.button.text = self:buttonText(arrowConfig)
                    end
                    return
                end
            end
        end
    end
end

function Level:fixOrUnfixObject(x, y)
    for i, golem in pairs(self.golems) do
        if golem.pos.x == x and golem.pos.y == y then
            golem.fixed = not golem.fixed
            return
        end
    end
    if self.arrowGrid.arrows[x][y] then
        local arrow = self.arrowGrid.arrows[x][y]
        arrow.fixed = not arrow.fixed
        return
    end
end


function Level:nextStep()
    -- increment steps counter
    self.stepsWalked = self.stepsWalked + 1
    -- decrement fruits' health
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.fruits[x][y] then
                self.fruits[x][y]:decrementHealth()
            end
        end
    end
end

function Level:draw()
    -- for compatibility with old draw functions that still use TileSize
    TileSize = 1
    
    -- apply a transform that accounts for TileSize and offsets
    love.graphics.push()
    love.graphics.applyTransform(self.gridTransform)
    
    -- draw the actual level content here:
    self.grid:draw()

    -- walls
    self.obstaclesGrid:draw()

    -- decoration (bottom layer)
    -- TODO

    -- foot tracks
    for _, golem in pairs(self.golems) do
        golem.tracks_grid:draw()
    end

    -- arrow signs
    self.arrowGrid:draw()

    -- draw homes
    Daylight:applyIllumColor(1, 1, 1, 1)
    local scale = 1.2 / Images.house:getHeight()
    for _ , home in ipairs(self.homes) do
        love.graphics.draw(Images.house, home.x - 1.2, home.y - 1.2, 0, scale, scale)
        --love.graphics.rectangle('fill', home.x - 0.875, home.y - 0.875, 0.75, 0.75)
    end

    -- draw plants and glowfruits
    for x = 1, self.grid.width do
        for y = 1, self.grid.height do
            if self.plants[x][y] then
                local plantScale = 0.5 / Images.plant1_base:getHeight()
                local plantX = x - 0.5
                local plantY = y - 1
                Daylight:applyReflColor(1, 1, 1, 1)
                love.graphics.draw(Images.plant1_base, plantX, plantY, 0, plantScale, plantScale) 
                if not self.harvestStarted then
                    Daylight:applyIllumColor(1, 1, 1, 0.7)
                    love.graphics.draw(Images.plant1_base_fruitlights, plantX, plantY, 0, plantScale, plantScale)
                    Daylight:applyReflColor(1, 1, 1, 1)
                    love.graphics.draw(Images.plant1_base_highlights, plantX, plantY, 0, plantScale, plantScale)
                    self.unripeFruit:draw(x, y)
                    Daylight:applyReflColor(1, 1, 1, 1)
                    love.graphics.draw(Images.plant1_front, plantX, plantY, 0, plantScale, plantScale)
                elseif self.fruits[x][y] and self.fruits[x][y].health > 1 then
                    Daylight:applyIllumColor(1, 1, 1, 1.0)
                    love.graphics.draw(Images.plant1_base_fruitlights, plantX, plantY, 0, plantScale, plantScale)
                    self.fruits[x][y]:draw(x, y)
                    Daylight:applyReflColor(1 ,1, 1, 0.7)
                    love.graphics.draw(Images.plant1_front, plantX, plantY, 0, plantScale, plantScale)
                elseif self.fruits[x][y] then
                    self.fruits[x][y]:draw(x, y)
                    Daylight:applyReflColor(1, 1, 1, 1)
                    love.graphics.draw(Images.plant1_front, plantX, plantY, 0, plantScale, plantScale)
                else
                    love.graphics.draw(Images.plant1_front, plantX, plantY, 0, plantScale, plantScale)
                end
            end
        end
    end

    -- characters
    for _, golem in pairs(self.golems) do
        golem:draw()
    end

    -- decoration (top layer)
    -- TODO

    self.gridCursor:draw()

    -- revert the transform because the following is not grid-based
    love.graphics.pop()

    if self.started and not self.lost and not self.won and not self.preWon then
        if self.harvestStarted then
            self.harvestUi:draw()
        else
            self.ui:draw()
        end
    end

    -- at last, draw text overlays for some special states:
    love.graphics.setFont(Fonts.default)
    Daylight:applyAbstractColor(1,1,1)


    if self.showMessage then -- messages in the gameplay
        DimRect()
        love.graphics.printf(self.showMessage, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
    elseif self.preWon == true and self.won == false then -- first step of outro
        DimRect()
        if self.preOutro ~= nil then
            love.graphics.printf(self.preOutro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
        end
    elseif self.won == true then -- second step of outro
        DimRect()
        if self.preOutro ~= nil then
            love.graphics.printf(self.preOutro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
        end
        love.graphics.printf(self.outro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/2, CANVAS_WIDTH * 0.8, "center")
    elseif self.lost == true then -- lost message
        DimRect()
        love.graphics.printf(self.lostMessage, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/2, CANVAS_WIDTH * 0.8, "center")
    elseif (self.started == false and self.intro ~= nil) then -- intro
        DimRect()
        love.graphics.printf(self.intro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT * 0.2, CANVAS_WIDTH * 0.8, "center")
    else
        -- any other conditions?
    end

end

function Level:start()
    self.started = true
end

function Level:isWon()
    return self.won
end

function Level:isLost()
    return self.lost
end

function Level:win()
    if not self.preWon then
        self.preWon = true
        Sounds.sigh:play()
        -- put logic for the start of the outro here
        -- (e.g. play a sound)
    end
end

function Level:reset(isHardReset)
    -- softReset: restore state before the harvest was started
    -- hardReset: undo everything the player has done

    self.harvestStarted = false
    Daylight:reset()
    for i, golem in ipairs(self.golems) do
        golem:reset()
    end

    -- default value for homes
    if not self.config.homes then
        self.config.homes = {{x=4,y=4}}
    end

    -- default basket size
    if not self.config.basket then
        self.config.basket = 6
    end

    if isHardReset then
        for i = #self.golems, 1, -1 do
            if not self.golems[i].fixed then
                table.remove(self.golems, i)
            end
        end

        for x = 1, self.arrowGrid.width do
            for y = 1, self.arrowGrid.height do
                local arrow = self.arrowGrid.arrows[x][y]
                if arrow and not arrow.fixed then
                    --self.arrowGrid.arrows[x][y] = nil
                    self:removeObject(x,y)
                end
            end
        end
    end

    self.homes = self.config.homes

    -- init plants grid
    self.plants = {}
    for x = 1, self.grid.width do
        self.plants[x] = {}
        for y = 1, self.grid.height do
            self.plants[x][y] = false
        end
    end

    if self.config.objects then
        for _, object in ipairs(self.config.objects) do
            self:placeFruit(object.x, object.y)
        end
    else
        -- default objects
        self:placeFruit(1, 2)
        self:placeFruit(1, 3)
        self:placeFruit(2, 2)
        self:placeFruit(2, 3)
        self:placeFruit(3, 2)
        self:placeFruit(3, 3)
    end

    self.remaining_fruits = self:countFruit()

    self.preWon = false
    self.won = false
    self.lost = false
    self.preWonTimer = self.preWonTime
    -- reset steps
    self.config.stepsLimit = self.config.stepsLimit or 12
    self.stepsWalked = 0
    self.stepTimeCounter = 0

    -- not setting 'started' to false here because this is also
    -- used for restarting and the intro should not be shown again then
end

function Level:getArrowGroups(skipCount)
    local arrowGroups = {}
    
    for x = 1, self.arrowGrid.width do
        for y = 1, self.arrowGrid.height do
            if self.arrowGrid.arrows[x][y] then
                local arrow = self.arrowGrid.arrows[x][y]
                local group = { -- new group, might be discarded if one with the same color already exists
                    color = arrow.color,
                    count = 0,
                    fixed = {},
                    isNew = true,
                }
                for _, existingGroup in pairs(arrowGroups) do
                    if sameColors(existingGroup.color, arrow.color) then
                        group = existingGroup
                        break
                    end
                end
                if arrow.fixed then
                    arrow.x = x
                    arrow.y = y
                    table.insert(group.fixed, arrow)
                elseif not skipCount then
                    group.count = group.count + 1
                end
                if group.isNew then
                    group.isNew = false
                    table.insert(arrowGroups, group)
                end
            end
        end
    end
    return arrowGroups
end

function Level:getGolemGroups(skipCount)
    -- the concept of golems changed last-minute, so there are no groups in theory
    -- let's just say there is always ONE group instead
    local group = {
        count = 0,
        fixed = {},
    }
    local golemGroups = { group }
    
    for _, golem in ipairs(self.golems) do
        if golem.fixed then
            table.insert(group.fixed, golem)
        elseif not skipCount then
            group.count = group.count + 1
        end
    end
    return golemGroups
end

-- go to next level if there is one, return false if not:
function NextLevel()
    if LevelManager.current < LevelManager.levelCount then
        -- got to next level
        LevelManager.current = LevelManager.current + 1
        -- reset previous level to not won (and not started)
        Levels[LevelManager.current - 1]:reset(true)
        if Levels[LevelManager.current - 1].intro ~= nil then
            Levels[LevelManager.current - 1].started = false
        end

        return true
    else
        return false
    end
end

-- initialize levels list:
Levels = {}

-- this must be called once after the levels have been loaded from files:
function InitLevelManager()
    LevelManager = {
        current = 1,
        levelCount = #Levels,
        currentLevel = function ()
            return Levels[LevelManager.current]
        end,
    }
end

return Level